defmodule TrixtaFlowsFlowsSupervisorTest do
  use ExUnit.Case
  doctest TrixtaFlows.FlowsSupervisor

  alias TrixtaFlows.{FlowServer, FlowsSupervisor}

  setup do
    id = UUID.uuid1()
    id1 = UUID.uuid1()

    flow = %TrixtaFlows.Flow{
      id: id,
      name: "Genèsis",
      description: "The Genèsis Flow"
    }

    child_spec = %{
      id: FlowServer,
      start: {FlowServer, :start_link, [flow]},
      restart: :transient
    }

    {:ok, pid} = start_supervised(FlowServer, child_spec)

    flow1 = %TrixtaFlows.Flow{
      id: id1,
      name: "Genèsis One",
      description: "The Genèsis One Flow"
    }

    child_spec1 = %{
      id: FlowServer1,
      start: {FlowServer, :start_link, [flow1]},
      restart: :transient
    }

    {:ok, pid1} = start_supervised(FlowServer, child_spec1)

    [flow: flow, flow_id: flow.id, pid: pid, flow1: flow1, flow1_id: flow1.id, pid1: pid1]
  end

  test "flows supervisor restarts flow on server crash", context do
    pid = context[:pid]
    ref = Process.monitor(pid)
    Process.exit(pid, :kill)

    receive do
      {:DOWN, ^ref, :process, ^pid, :killed} ->
        :timer.sleep(1)
        assert is_pid(TrixtaFlows.FlowServer.flow_pid(context[:flow_id]))
    after
      1000 ->
        raise :timeout
    end
  end

  test "can list active flows", _context do
    flow_ids_list = FlowsSupervisor.list_flow_ids()
    # assert flow_ids_list == []
    assert length(flow_ids_list) >= 2
  end

  test "can list inactive flows", _context do
    active_flows_list = FlowsSupervisor.list_flow_ids()
    assert TrixtaFlows.stop_flow(hd(active_flows_list)) == :ok
    inactive_flows_list = FlowsSupervisor.list_flow_ids(:inactive)
    all_flows_list = FlowsSupervisor.list_flow_ids(:all)
    assert length(inactive_flows_list) < length(all_flows_list)
  end

  test "can list all flows with details", _context do
    flows_list = FlowsSupervisor.list_flow_summaries()
    assert length(flows_list) > 1
    # assert flows_list == []
    assert hd(flows_list).name == "Genèsis One"
  end
end
