defmodule TrixtaFlowsFlowTest do
  use ExUnit.Case
  doctest TrixtaFlows.Flow

  alias TrixtaFlows.Flow

  setup do
    id = UUID.uuid1()

    init_state = %TrixtaFlows.Flow{
      id: id,
      name: "Genèsis",
      description: "The Genèsis Flow"
    }

    flow = Flow.new(init_state)

    [flow: flow]
  end

  test "new_flow returns the intial state" do
    id = UUID.uuid1()

    init_state = %TrixtaFlows.Flow{
      id: id,
      name: "Genèsis",
      description: "The Genèsis Flow"
    }

    flow = Flow.new(init_state)

    assert flow.id == id
    assert flow.name == "Genèsis"
    assert flow.description == "The Genèsis Flow"
  end

  test "new_flow returns the intial state with default values" do
    id = UUID.uuid1()

    init_state = %TrixtaFlows.Flow{
      id: id
    }

    flow = Flow.new(init_state)

    assert flow.id != nil
    assert flow.name =~ ~r/^[a-z]+-[a-z]+-[0-9]+/
    assert flow.description =~ ~r/.+/
  end

  test "can get flow summary", context do
    summary = Flow.summarize(context[:flow])

    assert summary.id != nil
    assert summary.name == "Genèsis"
    assert summary.description == "The Genèsis Flow"
  end

  test "can update Flow given valid complete new state", %{flow: flow} do
    new_state = %{
      id: flow.id,
      name: "Genèsis Two",
      description: "The Genèsis Two Flow",
    }

    {:ok, updated_flow} = Flow.update_state(flow, new_state)

    assert updated_flow.id == flow.id
    assert updated_flow.name == "Genèsis Two"
    assert updated_flow.description == "The Genèsis Two Flow"
  end

  test "can update Flow given valid partial new state", %{flow: flow} do
    new_state = %{
      name: "Genèsis Three"
    }

    {:ok, updated_flow} = Flow.update_state(flow, new_state)

    assert updated_flow.id == flow.id
    assert updated_flow.name == "Genèsis Three"
    assert updated_flow.description == "The Genèsis Flow"
  end
end
