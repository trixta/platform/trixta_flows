defmodule TrixtaFlowsFlowServerTest do
  use ExUnit.Case
  doctest TrixtaFlows.FlowServer

  alias TrixtaFlows.FlowServer

  setup do
    id = UUID.uuid1()

    flow = %TrixtaFlows.Flow{
      id: id,
      name: "Genèsis",
      description: "The Genèsis Flow"
    }

    child_spec = %{
      id: FlowServer,
      start: {FlowServer, :start_link, [flow]},
      restart: :transient
    }

    {:ok, pid} = start_supervised(FlowServer, child_spec)

    [flow: flow, id: id, pid: pid]
  end

  test "can get flow summary", %{id: id} do
    summary = FlowServer.summarize(id)

    assert summary.id != nil
    assert summary.name == "Genèsis"
    assert summary.description == "The Genèsis Flow"
  end

  test "can update Flow given valid complete new state", %{flow: flow} do
    new_state = %{
      id: flow.id,
      name: "Genèsis Two",
      description: "The Genèsis Two Flow"
    }

    {:ok, updated_flow} = FlowServer.update_state(flow.id, new_state)

    assert updated_flow.id == flow.id
    assert updated_flow.name == "Genèsis Two"
    assert updated_flow.description == "The Genèsis Two Flow"
  end

  test "can update Flow given valid partial new state", %{flow: flow} do
    new_state = %{
      name: "Genèsis Three"
    }

    {:ok, updated_flow} = FlowServer.update_state(flow.id, new_state)

    assert updated_flow.id == flow.id
    assert updated_flow.name == "Genèsis Three"
    assert updated_flow.description == "The Genèsis Flow"
  end
end
