defmodule TrixtaFlows.Service do
  defstruct(
    id: nil,
    name: nil,
    description: nil,
    current_state: nil,
    icon: nil,
    module_name: nil,
    last_remote_update_commit_id: nil,
    triggers: [],
    actions: []
  )

  def summarize(service) do
    %{
      id: service.id,
      name: service.name,
      description: service.description,
      current_state: service.current_state,
      icon: service.icon,
      module_name: service.module_name,
      last_remote_update_commit_id: service.last_remote_update_commit_id,
      triggers:
        Enum.map(service.triggers, fn a ->
          %{"module_name" => a["module_name"], "title" => a["title"]}
        end),
      actions:
        Enum.map(service.actions, fn a ->
          %{"module_name" => a["module_name"], "title" => a["title"]}
        end)
    }
  end
end
