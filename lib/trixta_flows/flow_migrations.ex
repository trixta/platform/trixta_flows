defmodule TrixtaFlows.FlowMigrations do
  @moduledoc """
  Responsible for converting old versions of the Flow struct into current versions,
  so that we can make breaking changes to the Flow struct while still supporting
  existing flows in running space's state.
  """

  alias TrixtaFlows.{FlowServer, FlowsSupervisor, FlowVersionManager}
  require Logger

  @doc """
    Migrates all non-system flows (those not tagged with 'trixta_sys') to the latest Flow struct version
    where necessary.
  """
  def migrate_all_non_system_flows() do
    TrixtaFlows.FlowsSupervisor.list_flow_summaries()
    |> Enum.reject(fn flow -> Enum.member?(flow.tags, "trixta_sys") end)
    |> Enum.map(fn flow -> migrate_flow(flow.id) end)
  end

  @doc """
  Loads an existing Flow struct from state, and then ensures that it is of the latest Flow struct format.
  The upgraded flow is saved into the flow process and dets, overwriting the old struct.
  """
  def migrate_flow(flow_id) when is_binary(flow_id) do
    case FlowsSupervisor.flow_details(flow_id) do
      [{^flow_id, existing_flow_definition}] ->
        updated_flow_struct = convert_to_latest_struct_version(existing_flow_definition)
        FlowServer.replace_state(flow_id, updated_flow_struct)
        :ok

      _ ->
        Logger.error(
          "Could not migrate flow #{flow_id} to the latest struct version, because the flow could not be found."
        )

        {:error, :flow_not_found}
    end
  end

  def convert_to_latest_struct_version(%{:nodes => _, :steps => _} = flow) do
    # Some old flows have both nodes and steps because of faulty merges.
    # Disregard the nodes and only use the steps.
    flow |> Map.delete(:nodes) |> convert_to_latest_struct_version()
  end

  def convert_to_latest_struct_version(%{:nodes => nodes} = flow) do
    # We have nodes but no steps. Rename the property.
    flow
    |> Map.put(:steps, nodes)
    |> Map.delete(:nodes)
    |> convert_to_latest_struct_version()
  end

  def convert_to_latest_struct_version(%{:steps => _} = flow) do
    # This is an old struct that is not versioning-aware,
    # because the steps are at the top level instead of being nested under a version.

    # Start with a blank flow struct (i.e. nil), and create a new one using details from this 
    # old struct to create the first initial version.
    FlowVersionManager.append_flow_version(nil, Map.delete(flow, :nodes))
  end

  def convert_to_latest_struct_version(%{:versions => _} = flow) do
    # This flow is already versioning-aware. It doesn't need transformation.
    flow
  end

  def convert_to_latest_struct_version(flow) do
    Logger.error(
      "Could not figure out the struct version of the given flow. Not attempting any transformations. Flow struct given: #{
        inspect(flow)
      }"
    )

    flow
  end
end
