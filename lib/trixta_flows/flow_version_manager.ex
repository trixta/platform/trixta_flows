defmodule TrixtaFlows.FlowVersionManager do
  @moduledoc """
  Handles the compiling and deleting of flow modules for the various versions of a flow.
  """
  require Logger
  alias TrixtaFlows.{FlowsSupervisor, FlowServer}

  @doc """
  If no flow exists for the given flow_id, start one with the given version_definition as initial version.
  If the flow already exists, overwrite its latest version with the given version_definition.
  """
  def start_flow_or_overwrite_latest_version(flow_id, version_definition)
      when is_binary(flow_id) do
    case FlowsSupervisor.flow_details(flow_id) do
      [{^flow_id, %TrixtaFlows.Flow{} = _existing_flow_definition}] ->
        # The flow already exists.
        # First overwrite any top-level fields (outside of the versions) that we may want updated:
        updated_root_fields = Map.delete(version_definition, :versions)
        FlowServer.update_state(flow_id, updated_root_fields)

        # Now overwrite the latest version with the definition provided, or create a first version if none exists.
        overwrite_latest_version(flow_id, version_definition)

      _ ->
        # Flow does not exist yet. Start a new one.
        case append_flow_version(nil, version_definition)
             |> TrixtaFlows.start_flow() do
          {:ok, _, _flow_result} ->
            :started_new_flow

          err ->
            Logger.error("Could not start new flow called #{flow_id} . Details: #{inspect(err)}")
            {:error, err}
        end
    end
  end

  @doc """
  Returns a tuple of {flow_struct, latest_version_struct},
  where latest_version_struct is the latest FlowVersion by created_timestamp.

  If the flow is not found, :flow_not_found is returned.
  If no versions are found, {flow_struct, nil} is returned.
  """
  def get_latest_version(flow_id) when is_binary(flow_id) do
    case FlowsSupervisor.flow_details(flow_id) do
      [{^flow_id, %TrixtaFlows.Flow{} = flow_struct}] ->
        {flow_struct, get_latest_version(flow_struct)}

      [_first | _rest] ->
        :multiple_flows_found

      _ ->
        :flow_not_found
    end
  end

  @doc """
  Returns the latest FlowVersion of a given flow by created_timestamp.
  """
  def get_latest_version(%TrixtaFlows.Flow{:versions => versions} = _flow_struct) do
    case versions
         |> Map.values()
         |> Enum.max_by(fn vsn -> vsn.created_timestamp end, &>=/2, fn -> nil end) do
      %TrixtaFlows.FlowVersion{} = flow_version ->
        flow_version

      _ ->
        :no_flow_versions_found
    end
  end

  @doc """
  If the flow definition is in legacy format without versions explicitly defined,
  this function converts it, using the top-level steps as the first version.
  """
  def get_latest_version(flow) do
    [flow]
    |> assemble_flow_versions()
    |> get_latest_version()
  end

  def get_latest_version_steps(flow_id) do
    TrixtaFlows.FlowVersionManager.get_latest_version(flow_id) |> elem(1) |> Map.get(:steps)
  end

  def get_flow_version(flow_id, version_name) do
    with [{_flow_name, %TrixtaFlows.Flow{} = flow_definition} | _rest] <-
           FlowsSupervisor.flow_details(flow_id),
         %{:steps => _} = flow_version <- flow_definition.versions |> Map.get(version_name) do
      {:ok, flow_definition, flow_version}
    else
      [] -> :flow_not_found
      nil -> :flow_version_not_found
    end
  end

  @doc """
  Sets the given flow version on the flow struct in space state.
  This is used, for e.g., when we want to pre-load a specific version of a flow from Gitlab
  and load it into space state for compilation.
  """
  def set_flow_version(%TrixtaFlows.Flow{} = flow, version_definition) do
    updated_flow = append_flow_version(flow, version_definition)
    FlowServer.update_state(flow.id, updated_flow)
  end

  def set_flow_version(flow_id, flow_version) do
    with [{_flow_name, %TrixtaFlows.Flow{} = flow_definition} | _rest] <-
           FlowsSupervisor.flow_details(flow_id) do
      set_flow_version(flow_definition, flow_version)
    else
      [] -> :flow_not_found
    end
  end

  def set_flow_version(flow_id, version_name, version_details) when is_binary(version_name) do
    set_flow_version(flow_id, Map.put(version_details, :version_name, version_name))
  end

  def set_flow_version(flow_id, _, version_details) do
    # Version name is not a string, so don't pass it.
    set_flow_version(flow_id, version_details)
  end

  def delete_all_local_versions(flow_id) do
    TrixtaFlows.FlowVersionManager.get_local_flow_versions(flow_id)
    |> Enum.map(fn item -> Map.get(item, "version_name") end)
    |> Enum.map(fn local_flow_version ->
      case TrixtaFlows.FlowsSupervisor.flow_details(flow_id) do
        [{^flow_id, %TrixtaFlows.Flow{} = existing_flow_definition}] ->
          definition_without_versions =
            append_flow_version(
              nil,
              Map.delete(existing_flow_definition, :versions)
            )
            |> Map.put(
              :source,
              Map.get(existing_flow_definition, :source)
            )

          FlowServer.update_state(flow_id, definition_without_versions)
          {:ok, %{}}

        _ ->
          {:error, %{"message" => "Could not find flow called #{flow_id}"}}
      end
    end)
  end

  @doc """
  Fetches a list of versions available for the given flow as stored in space state.
  Does not look on Gitlab.
  """
  def get_local_flow_versions(flow_id) do
    with [{_flow_name, %TrixtaFlows.Flow{} = flow_definition} | _rest] <-
           FlowsSupervisor.flow_details(flow_id) do
      flow_definition.versions
      |> Enum.map(fn {version_name, %TrixtaFlows.FlowVersion{} = version_definition} ->
        {:ok, flow_definition, _, _} = extract_flow_version(flow_definition, version_name)

        %{
          "flow_id" => flow_id,
          "version_name" => version_name,
          "created_timestamp" => version_definition.created_timestamp,
          "version_description" => version_definition.version_description,
          "flow_definition" =>
            TrixtaFlowCode.DefinitionToJsonEncodable.to_json_encodable(flow_definition)
        }
      end)
    else
      _ -> {:error, :flow_not_found}
    end
  end

  @doc """
  Compiles a specific version of the flow into a sub-namespace of the main flow context.
  If the version was already compiled, it is replaced.

  CAUTION: Be careful when re-defining existing versions, because any flows currently running
  under the existing version will fail as the existing step modules are purged before re-compiling.
  Rather try to create a new version if something has to change in the flow.
  """

  def compile_version(flow, flow_version, overwrite \\ false)

  def compile_version(
        %TrixtaFlows.Flow{} = flow,
        %TrixtaFlows.FlowVersion{} = flow_version,
        overwrite
      ) do
    if not is_version_compiled?(flow, flow_version) or overwrite do
      delete_flow_modules(flow, flow_version)

      defmodule flow_module(flow, flow_version) do
        use Trixta.Steps.Flow.V_0_0_1
        dna(flow, flow_version)
      end
    end
  end

  def compile_version(flow_id, version_name, overwrite)
      when is_binary(flow_id) and is_binary(version_name) do
    with [{_flow_name, %TrixtaFlows.Flow{} = flow_definition} | _rest] <-
           FlowsSupervisor.flow_details(flow_id),
         %TrixtaFlows.FlowVersion{:steps => _} = version_definition <-
           flow_definition.versions[version_name] do
      {:ok, compile_version(flow_definition, version_definition, overwrite)}
    else
      [] ->
        Logger.error("Flow #{flow_id} not found while trying to compile version #{version_name}")
        {:error, :flow_not_found}

      _ ->
        Logger.error(
          "Could not find version #{version_name} on flow #{flow_id} in order to recompile it."
        )

        {:error, :version_not_found}
    end
  end

  def compile_version(%TrixtaFlows.Flow{} = flow, version_name, overwrite) do
    compile_version(flow.id, version_name, overwrite)
  end

  def is_version_compiled?(%TrixtaFlows.Flow{} = flow, %TrixtaFlows.FlowVersion{} = flow_version) do
    function_exported?(root_step_module(flow, flow_version), :__info__, 1)
  end

  @doc """
  Deletes all compiled modules for the given flow and ALL its versions, including its parent module.
  """
  def delete_flow_modules(%TrixtaFlows.Flow{} = flow) do
    flow.versions |> Enum.each(fn {_, vsn} -> delete_flow_modules(flow, vsn) end)
  end

  @doc """
  Deletes the compiled modules (including steps) for the given flow version.
  """
  def delete_flow_modules(%TrixtaFlows.Flow{} = flow, %TrixtaFlows.FlowVersion{} = flow_version) do
    flow_version.steps
    |> Enum.each(fn {step_module, _} ->
      step_mod = step_module(flow, flow_version, step_module)
      :code.delete(step_mod)
      :code.purge(step_mod)
    end)

    flow_mod = flow_module(flow, flow_version)
    :code.delete(flow_mod)
    :code.purge(flow_mod)
  end

  def flow_module(%{:name => flow_name}) do
    Module.concat([
      Elixir.TrixtaFlows.Compiled,
      Macro.camelize(flow_name)
    ])
  end

  def flow_module(flow, %{:version_name => version_name} = _flow_version)
      when is_binary(version_name) do
    Module.concat([
      flow_module(flow),
      Versions,
      Macro.camelize(version_name)
    ])
  end

  def flow_module(_flow, flow_version) do
    raise "Could not determine flow module name for flow version: #{inspect(flow_version)}"
  end

  def root_step_module(%TrixtaFlows.Flow{} = flow, %TrixtaFlows.FlowVersion{} = flow_version) do
    step_module(flow, flow_version, flow_version.root_step)
  end

  def step_module(flow, flow_version, step_module_name) when is_binary(step_module_name) do
    step_module(flow, flow_version, Macro.camelize(step_module_name) |> String.to_atom())
  end

  def step_module(
        %TrixtaFlows.Flow{} = flow,
        %TrixtaFlows.FlowVersion{} = flow_version,
        step_module_name
      )
      when is_atom(step_module_name) do
    Module.concat([
      flow_module(flow, flow_version),
      step_module_name
    ])
  end

  def assemble_flow_versions(%TrixtaFlows.Flow{:versions => versions} = flow)
      when is_map(versions) and map_size(versions) > 0 do
    # This is already a flow struct saved in the correctly versioned format. Doesn't need conversion.
    flow
  end

  @doc """
  Takes a list of flow maps with :steps at the top level,
  and assembles them into a single TrixtaFlows.Flow struct
  with the versions listed under the :versions map.
  Conflicts in the main flow name, etc are resolved by using the first one in the list.
  """
  def assemble_flow_versions([_ | _] = versions) do
    versions
    |> Enum.reduce(nil, fn version, acc_flow ->
      append_flow_version(acc_flow, version)
    end)
  end

  def assemble_flow_versions(%{:steps => _} = first_version) do
    # We have an ordinary map representing a single flow version.
    # Turn it into a flow struct with the given version as the first one.
    [first_version] |> assemble_flow_versions()
  end

  @doc """
  Returns a single, unversioned flow map with only the latest version at the top level.
  """
  def extract_flow_version(flow_id) when is_binary(flow_id) do
    case get_latest_version(flow_id) do
      {%TrixtaFlows.Flow{} = flow_struct, %TrixtaFlows.FlowVersion{} = latest_version_struct, _,
       _} ->
        extract_flow_version(flow_struct, latest_version_struct.version_name)

      _ ->
        # Flow not found or multiple flows found for this id.
        nil
    end
  end

  @doc """
  Returns a single, unversioned flow map with only the latest version at the top level.
  """
  def extract_flow_version(%TrixtaFlows.Flow{:versions => %{}} = flow_struct) do
    case get_latest_version(flow_struct) do
      %TrixtaFlows.FlowVersion{:version_name => latest_version_name} ->
        extract_flow_version(flow_struct, latest_version_name)

      err ->
        err
    end
  end

  def extract_flow_version(flow_id, version_name) when is_binary(flow_id) do
    case FlowsSupervisor.flow_details(flow_id) do
      [{_, flow}] ->
        extract_flow_version(flow, version_name)

      _ ->
        Logger.error(
          "Could not extract version #{version_name} from flow #{flow_id} . Flow not found."
        )

        {:error, :flow_not_found}
    end
  end

  @doc """
  Picks a specific version from flow state and returns a map with only that version,
  so that it may be saved to version control e.g. Gitlab.
  On Gitlab, versions are denoted by the file's history, and not in the contents of any individual file ref.
  """
  def extract_flow_version(flow, version_name) do
    case flow do
      %TrixtaFlows.Flow{
        :versions => %{^version_name => %TrixtaFlows.FlowVersion{:steps => _} = version}
      } ->

        # Flatten the structure so that the given version's fields are at the top level of the map saved on Gitlab.
        {:ok,
         %{
           id: flow.id,
           name: flow.name,
           description: flow.description,
           tags: Map.get(flow, :tags, []),
           steps: version.steps,
           root_step: version.root_step,
           flow_engine_version: version.flow_engine_version,
         }, version_name, version.created_timestamp}

      _ ->
        Logger.error(
          "Could not extract version #{version_name} from flow. Version not found or invalid flow struct: #{
            inspect(flow)
          }"
        )

        {:error, :version_not_found}
    end
  end

  @doc """
  Picks a specific version from flow state and returns a map with the json-encodable definition,
  the version name and the created timestamp for that version.
  """
  def extract_flow_version_json_encodable(flow_id, version_name) do
    case extract_flow_version(flow_id, version_name) do
      {:ok, %{:steps => _} = flow, ^version_name, version_timestamp} ->
        {:ok,
         %{
           "flow" => TrixtaFlowCode.DefinitionToJsonEncodable.to_json_encodable(flow),
           "version_name" => version_name,
           "version_timestamp" => version_timestamp
         }}

      {:error, :flow_not_found} ->
        {:error, :flow_not_found}

      {:error, :version_not_found} ->
        {:error, :version_not_found}

      other ->
        Logger.error(
          "Could not extract and decode version #{version_name} of flow #{flow_id} . extract_flow_version/2 returned: #{
            inspect(other)
          }"
        )

        {:error, :cannot_extract_version}
    end
  end

  # If the flow has versions, overwrite the latest one.
  # If it doesn't have any versions, create a new one.
  defp overwrite_latest_version(%TrixtaFlows.Flow{} = flow, version_details) do
    case get_latest_version(flow) do
      %TrixtaFlows.FlowVersion{} = latest_version ->
        case set_flow_version(
               flow,
               Map.put(version_details, :version_name, latest_version.version_name)
             ) do
          {:ok, _} ->
            # Recompile the latest version:
            FlowServer.compile_version(flow.id, latest_version.version_name, true)
            {:overwrote_latest_version, latest_version.version_name}

          err ->
            Logger.error(
              "Error overwriting latest version of flow #{flow.id} . Details: #{inspect(err)}"
            )

            {:error, err}
        end

      _ ->
        :no_flow_versions_found
    end
  end

  defp overwrite_latest_version(flow_id, new_version_details) when is_binary(flow_id) do
    case FlowsSupervisor.flow_details(flow_id) do
      [{^flow_id, flow_struct}] ->
        overwrite_latest_version(flow_struct, new_version_details)

      _ ->
        Logger.error(
          "Could not overwrite latest version of flow #{inspect(flow_id)} . Flow not found."
        )

        {:error, :flow_not_found}
    end
  end

  # The input is already a versioned flow struct. We don't have to process it.
  def append_flow_version(nil, %{:versions => _} = flow_struct) do
    flow_struct
  end

  # Initializes a new TrixtaFlows.Flow struct with the given version as the first one.
  def append_flow_version(nil, version) do
    version_name = flow_version_name(version)

    %TrixtaFlows.Flow{
      id: version.id,
      # This is the flow's main name, not the individual version's name.
      name: version.name,
      root_step: version.root_step,
      # This is the flow's main description, not the individual version's name.
      description: version.description,
      tags: Map.get(version, :tags, []),
      versions: %{
        version_name => flow_version_struct(version |> Map.put(:version_name, version_name))
      }
    }
  end

  def append_flow_version(%TrixtaFlows.Flow{} = flow, version) do
    version_name = flow_version_name(version)

    %{
      flow
      | versions:
          Map.put(
            flow.versions,
            version_name,
            flow_version_struct(Map.put(version, :version_name, version_name))
          )
    }
  end

  # The version name defailts to the current UTC timestamp if none is given.
  defp flow_version_name(version) do
    case version do
      %{:version_name => name} -> name
      _ -> "initial_#{UUID.uuid4(:hex)}"
    end
  end

  # Some old flows still use 'version' to mean 'flow engine version'.
  # TODO: When the old flows have been upgraded, remove this function.
  defp flow_engine_version(version) do
    case version do
      %{:flow_engine_version => version} -> version
      %{:version => version} -> version
      # Not sure what the default should be, but most flows currently have '2' .
      _ -> "2"
    end
  end

  defp flow_version_struct(%{:nodes => nodes} = version) do
    # Some old flows still put steps under the :nodes key.
    # TODO: Remove this once all flows have been upgraded.
    version
    |> Map.delete(:nodes)
    |> Map.put(:steps, nodes)
    |> flow_version_struct()
  end

  defp flow_version_struct(%{:steps => steps} = version) do
    %TrixtaFlows.FlowVersion{
      version_name: version.version_name,
      source: version |> Map.get(:source, nil),
      steps: steps,
      flow_engine_version: flow_engine_version(version),
      root_step: version.root_step,
      created_timestamp: Map.get(version, :created_timestamp, to_string(DateTime.utc_now())),
      should_save_to_git: Map.get(version, :should_save_to_git, false),
      version_description: Map.get(version, :version_description, nil)
    }
  end

  defp flow_version_struct(version) do
    Logger.error("Could not build FlowVersion struct from the given input #{inspect(version)}")
    nil
  end
end
