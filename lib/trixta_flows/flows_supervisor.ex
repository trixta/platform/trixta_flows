defmodule TrixtaFlows.FlowsSupervisor do
  @moduledoc """
  A supervisor that starts `FlowServer` processes dynamically.
  """

  use DynamicSupervisor

  require Logger

  alias TrixtaFlows.FlowServer

  def start_link(_arg) do
    DynamicSupervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  @doc """
  Starts a `FlowServer` process given a Flow struct and supervises it.
  """
  def start_flow(flow) do
    child_spec = %{
      id: FlowServer,
      start: {FlowServer, :start_link, [flow]},
      restart: :transient
    }

    DynamicSupervisor.start_child(__MODULE__, child_spec)
  end

  @doc """
  Stop the `FlowServer` process normally. It CAN be restarted.
  """
  def stop_flow(flow_id) do
    with child_pid when not is_nil(child_pid) <- FlowServer.flow_pid(flow_id) do
      DynamicSupervisor.terminate_child(__MODULE__, child_pid)
      Logger.info("Stopped flow server process with ID '#{flow_id}'.")
    end
  end

  @doc """
  Terminates the `FlowServer` process AND state normally. It CAN'T be restarted.
  """
  def terminate_flow(flow_id) do
    with child_pid when not is_nil(child_pid) <- FlowServer.flow_pid(flow_id) do
      DynamicSupervisor.terminate_child(__MODULE__, child_pid)
      Logger.info("Stopped flow server process with ID '#{flow_id}'.")
    end

    TrixtaStorage.delete(:flows_table, flow_id)
    FastGlobal.put("flow_#{flow_id}", nil)
    Logger.info("TERMINATING flow server process and state with ID '#{flow_id}'...")
  end

  @doc """
  List Flows
  """
  def list_flow_ids() do
    list_flow_ids(:processes)
  end

  def list_flow_ids(:processes) do
    :global.registered_names()
    |> Enum.filter(fn {module, _name} -> module == TrixtaFlows.FlowRegistry end)
    |> Enum.reduce([], fn {_, name}, names -> [name | names] end)
    |> Enum.sort()
  end

  def list_flow_ids(:all) do
    TrixtaStorage.traverse(:flows_table, fn {flow_id, _flow} ->
      {:continue, flow_id}
    end)
    |> Enum.sort()
  end

  def list_flow_ids(:inactive) do
    MapSet.difference(MapSet.new(list_flow_ids(:all)), MapSet.new(list_flow_ids(:processes)))
    |> MapSet.to_list()
  end

  def list_flow_ids(:difference) do
    MapSet.difference(MapSet.new(list_flow_ids(:all)), MapSet.new(list_flow_ids(:processes)))
    |> MapSet.to_list()
  end

  def list_flow_ids(:myers_difference) do
    List.myers_difference(list_flow_ids(:processes), list_flow_ids(:all))
  end

  @doc """
  List Flow summaries
  """
  def list_flow_summaries() do
    TrixtaStorage.traverse(:flows_table, fn {_flow_id, flow} ->
      {:continue,
       %{
         :id => flow.id,
         :name => flow.name,
         :version => Map.get(flow, :version, "1"),
         :tags => Map.get(flow, :tags, [])
       }}
    end)
  end

  @doc """
  List Service summaries
  """
  def list_service_summaries() do
    TrixtaStorage.traverse(:services_table, fn {_service_id, service} ->
      {:continue,
       %{
         :name => service.name,
         :id => service.id,
         "triggers" =>
           List.wrap(service.triggers)
           |> Enum.map(fn trigger ->
             %{
               "title" => trigger["title"],
               "id" => trigger["guid"],
               "module_name" => trigger["module_name"],
               "outcomes" => outcome_summary(trigger["outcomes"])
             }
           end),
         "actions" =>
           List.wrap(service.actions)
           |> Enum.map(fn actions ->
             %{
               "title" => actions["title"],
               "id" => actions["guid"],
               "module_name" => actions["module_name"],
               "outcomes" => outcome_summary(actions["outcomes"])
             }
           end)
       }}
    end)
  end

  def all_services() do
    TrixtaStorage.traverse(:services_table, fn {_service_id, service} ->
      {:continue, service}
    end)
  end

  @doc """
  Get Flow details by flow_id
  """
  def flow_details(flow_id) do
    case FastGlobal.get("flow_#{flow_id}", nil) do
      nil ->
        storage_result = TrixtaStorage.lookup(:flows_table, flow_id)
        FastGlobal.put("flow_#{flow_id}", storage_result)
        storage_result

      result ->
        result
    end
  end

  defp outcome_summary(outcomes) do
    List.wrap(outcomes)
    |> Enum.map(fn outcome ->
      %{
        "name" => outcome["name"],
        "description" => outcome["description"]
      }
    end)
  end
end
