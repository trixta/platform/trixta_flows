defmodule TrixtaFlows.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: TrixtaFlows.Worker.start_link(arg)
      # {TrixtaFlows.Worker, arg},
      TrixtaFlows.FlowsSupervisor,
      {Registry, [keys: :duplicate, name: :outbound_reactions]},
      {Registry, [keys: :duplicate, name: :running_flows]}
    ]

    TrixtaStorage.open_table(:trixta_flows, :flows_table)

    # Note: due to not being sure whether we even want a services application, short term resolution is to use the flows application for services
    TrixtaStorage.open_table(:trixta_flows, :services_table)

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: TrixtaFlows.Supervisor]
    ret = Supervisor.start_link(children, opts)

    # load any existing flows
    TrixtaStorage.traverse(:flows_table, fn {_flow_id, flow} ->
      case TrixtaFlows.start_flow(flow) do
        {:error, {:already_started, _pid}} -> :continue
        {:error, {:unknown_error}, _} -> :continue
        {:ok, _pid, _flow} -> :continue
      end
    end)

    ret
  end
end
