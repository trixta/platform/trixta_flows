defmodule TrixtaFlows.FlowVersion do
  @moduledoc """
  Represents a particular version of a flow.
  """
  defstruct(
    version_name: nil,
    # Version description is what gets included in the commit message when pushing the version to Gitlab etc.
    version_description: nil,
    # We might only want certain major versions to be saved on Gitlab.
    should_save_to_git: false,
    # Source contains details about where the flow version came from (e.g. a specific ref on gitlab. By default, the system assumes this to be a Gitlab tag.)
    source: %{},
    steps: nil,
    current_state: :initialising,
    flow_engine_version: "1",
    root_step: nil,
    created_timestamp: nil
  )
end
