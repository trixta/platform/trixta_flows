defmodule TrixtaFlows.Flow do
  defstruct(
    id: nil,
    name: nil,
    description: "New Flow",
    # A map of TrixtaFlows.FlowVersion structs representing the various versions of this flow and their definitions,
    # keyed by the version name.
    # The version's created_timestamp property indicates what the latest version is.
    versions: %{},
    # Source contains details about where the flow came from (e.g. Gitlab)
    source: %{},
    steps: %{},
    root_step: nil,
    # Tags is an array of strings, lowercase
    tags: []
  )

  def summarize(flow) do
    %{
      id: flow.id,
      name: flow.name,
      description: flow.description,
      tags: Map.get(flow, :tags, [])
    }
  end

  def new(%TrixtaFlows.Flow{id: nil}) do
    raise ArgumentError, message: "the argument ID is invalid"
  end

  def new(flow = %TrixtaFlows.Flow{}) do
    flow =
      if flow.name do
        flow
      else
        %{flow | name: TrixtaNames.generate()}
      end

    flow
  end

  def update_state(flow, %TrixtaFlows.Flow{} = new_state) do
    new_state
    |> Map.from_struct()
    |> (fn new_state_map -> update_state(flow, new_state_map) end).()
  end

  def update_state(flow, new_state) do
    new_state =
      new_state
      |> Map.new(fn {k, v} ->
        case is_atom(k) do
          false -> {String.to_existing_atom(k), v}
          true -> {k, v}
        end
      end)

    validate(Map.merge(flow, new_state))
  end

  # TODO: Can validate any whole or partial new space state
  # **Validation needs to happen here**
  defp validate(new_state) do
    {:ok, new_state}
    # valid? = {:ok, new_state}

    # case valid? do
    #   {:ok, new_state} ->
    #     {:ok, new_state}

    #   {:error, messages} ->
    #     {:error, messages}
    # end
  end
end
