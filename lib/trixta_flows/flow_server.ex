defmodule TrixtaFlows.FlowServer do
  @moduledoc """
  A flow server process that holds a `Flow` struct as its state.
  """

  use GenServer

  require Logger

  alias TrixtaFlows.{Flow, FlowVersion, FlowVersionManager}

  @backup_interval :timer.minutes(5)

  # Client (Public) Interface

  @doc """
  Spawns a new flow server process registered under the given `flow_id`.
  """
  def start_link(flow) do
    GenServer.start_link(
      __MODULE__,
      {flow},
      name: via_tuple(flow.id)
    )
  end

  def summarize(flow_id) do
    GenServer.call(via_tuple(flow_id), :summary)
  end

  def update_state(flow_id, %{} = new_state) do
    GenServer.call(via_tuple(flow_id), {:update_state, new_state}, 60000)
  end

  @doc """
  WARNING: Replaces the GenServer / TrixtaStorage state with ANYTHING.
  Used ONLY when migrating flow sturct versions and for testing.
  Don't use this during normal operation.
  """
  def replace_state(flow_id, new_state) do
    GenServer.call(via_tuple(flow_id), {:replace_state, new_state})
  end

  def compile_version(flow_id, flow_version, overwrite \\ false)
      when is_binary(flow_id) do
    GenServer.call(via_tuple(flow_id), {:compile_version, flow_version, overwrite}, 60_000)
  end

  @doc """
  Returns a tuple used to register and lookup a flow server process by flow_id.
  """
  def via_tuple(flow_id) when is_pid(flow_id) do
    flow_id
  end

  def via_tuple(flow_id) do
    {:via, :global, {TrixtaFlows.FlowRegistry, flow_id}}
  end

  @doc """
  Returns the `pid` of the flow server process registered under the given `flow_id`, or `nil` if no process is registered.
  """
  def flow_pid(flow_id) do
    flow_id
    |> via_tuple()
    |> GenServer.whereis()
  end

  # Server Callbacks

  def init(flow) do
    {flow} = flow
    flow_id = flow.id

    flow =
      case TrixtaStorage.lookup(:flows_table, flow.id) do
        [] ->
          new_flow = Flow.new(flow)
          TrixtaStorage.save(:flows_table, flow.id, new_flow)
          FastGlobal.put("flow_#{flow.id}", [{flow.id, new_flow}])
          new_flow

        [{^flow_id, flow}] ->
          Logger.info("Flow with ID '#{flow_id}' already exisits. Restarting...")
          flow
      end

    Logger.info("Spawned flow server process for flow ID '#{flow.id}'.")

    schedule_backup()
    {:ok, flow}
  end

  def handle_call(:summary, _from, flow) do
    {:reply, Flow.summarize(flow), flow}
  end

  def handle_call({:update_state, new_state}, _from, flow) do
    case Flow.update_state(flow, new_state) do
      {:ok, new_flow} ->
        flow_id = my_flow_id()
        TrixtaStorage.save(:flows_table, flow_id, new_flow)
        FastGlobal.put("flow_#{flow_id}", [{flow_id, new_flow}])
        {:reply, {:ok, Flow.summarize(new_flow)}, new_flow}

      {:error, messages} ->
        {:reply, {:error, messages}}
    end
  end

  @doc """
  WARNING: Replaces the GenServer / TrixtaStorage state with ANYTHING.
  Used ONLY when migrating flow sturct versions and for testing.
  Don't use this during normal operation.
  """
  def handle_call({:replace_state, new_state}, _from, flow) do
    flow_id = my_flow_id()
    TrixtaStorage.save(:flows_table, flow_id, new_state)
    FastGlobal.put("flow_#{flow_id}", [{flow_id, new_state}])
    {:reply, {:ok, new_state}, new_state}
  end

  @doc """
  Compiles the given flow version if it isn't already compiled, or if overwrite is true.
  This has to be done through the flow's GenServer to prevent a race condition between the 'is compiled' check and the actual compilation,
  which may lead to 'cannot redefine module' errors.
  """
  def handle_call({:compile_version, flow_version, overwrite}, _from, flow) do
    {:reply, FlowVersionManager.compile_version(flow, flow_version, overwrite), flow}
  end

  def handle_info(:backup, flow) do
    # TODO: Do backup of the state here
    # Logger.info("State for flow server process with flow ID '#{flow.id}' would be backed up now.")

    schedule_backup()
    {:noreply, flow}
  end

  defp my_flow_id do
    ids = :global.registered_names()

    id =
      Enum.find_value(ids, fn id ->
        if :global.whereis_name(id) == self(), do: id
      end)

    {_, id} = id
    id
  end

  defp schedule_backup() do
    Process.send_after(self(), :backup, @backup_interval)
  end
end
