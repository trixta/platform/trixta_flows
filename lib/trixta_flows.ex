defmodule TrixtaFlows do
  alias TrixtaFlows.FlowVersionManager

  def start_flow() do
    start_flow(%TrixtaFlows.Flow{id: UUID.uuid1()})
  end

  def start_flow(%TrixtaFlows.Flow{id: nil} = flow) do
    flow = Map.put(flow, :id, UUID.uuid1())
    start_flow(flow)
  end

  def start_flow(%TrixtaFlows.Flow{} = flow) do
    case TrixtaFlows.FlowsSupervisor.start_flow(flow) do
      {:ok, pid} ->
        {:ok, pid, TrixtaFlows.FlowServer.summarize(pid)}

      {:error, _} = error ->
        error

      _ ->
        {:error, {:unknown_error}, flow: flow}
    end
  end

  # If `flow` is a plain map then atomize keys and structify
  def start_flow(%{} = flow) do
    flow
    |> Map.new(fn {k, v} ->
      case is_atom(k) do
        false -> {String.to_existing_atom(k), v}
        true -> {k, v}
      end
    end)
    |> FlowVersionManager.assemble_flow_versions()
    |> start_flow()
  end

  def start_flow(flow_id) do
    start_flow(%TrixtaFlows.Flow{id: flow_id})
  end

  def stop_flow(flow_id) do
    TrixtaFlows.FlowsSupervisor.stop_flow(flow_id)
  end

  def terminate_flow(flow_id) do
    TrixtaFlows.FlowsSupervisor.terminate_flow(flow_id)
  end
end
